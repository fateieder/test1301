<?php
namespace app\components\CategoryMenu;

use yii\helpers\Url;

class Menu
{
    public static $data;


    public static function usedData($id){
        foreach (self::$data as $key=>$item){
            if ($item['id'] == $id) {
                self::$data[$key]['used'] = true;
            }
        }
    }

    public static function setData($data){
        self::$data = $data;
    }
    /**
     * Для вывода древовидного меню.
     * @return array
     */

    public static function getMenu()
    {
       $sql = 'SELECT category.*, count(news.id) as sum_news FROM category LEFT JOIN news ON category.id = news.category_id group by category.id';
        $data = \Yii::$app->db->createCommand($sql)
            ->queryAll();

        /**
         * Создаем флаг в массиве об использовании этой категории. Против циклической завязки
         */
        foreach ($data as $key => $item) {
            $data[$key]['used'] = false;
        }
        self::setData($data);
        return self::getItemMenu(0);
    }

    /**
     * Для рекурсии с целью построения меню.
     * @param int $parent
     * @return array
     */
    public static function getItemMenu($parent = 0)
    {
        /**
         * Пометка, что данный родитель используется
         */
        self::usedData($parent);
        /**
         * Перебираем всех и ищем у кого данный родитель.
         */
        foreach (self::$data as $key => $item) {
            /** Если родитель у текущего элемента и текущий элемент не использовался и количество новостей не 0 или есть подкатегории*/
            if ($parent == $item['parent_id'] && (!$item['used']) && (($item['sum_news'] != 0) || count($items = self::getItemMenu($item['id'])))) {
                $result[] = [
                    'label' => $item['name'],
                    'url' => (($item['sum_news'] != 0) ? (Url::to('/category/' . $item['id'])) : ("#")),
                    'items' => $items,
                ];
                unset($items);
            }
        }
        return $result;
    }
}