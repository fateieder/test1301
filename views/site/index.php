<?php

/* @var $this yii\web\View */

?>
<div class="site-index">

    <div class="body-content">
        <h1><?= \yii\helpers\Html::encode($this->title) ?></h1>
        <div class="row">
            <?= \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
                    return $this->render('/news/_list_news', ['model' => $model]);
                },
            ]) ?>
        </div>

    </div>
</div>
