<?php
/**
 * @var $model \app\models\News
 */
?>
<div class="col-lg-4 text-info" style="border: thin solid #67b460">
    <?php
    if ($model->image) { ?>
        <img src="/images/<?= $model->image; ?>" style="width: 100%;">
    <?php } ?>
    <p>
        <?= $model->name; ?>
    </p>
    <p>
        <?= $model->preview; ?>
    </p>
    <?= \yii\helpers\Html::a("Читать далее", ['/news/' . $model->url_title]); ?>
</div>
