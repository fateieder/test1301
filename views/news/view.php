<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php
    echo $this->render('_item_news', ['model' => $model]);
    echo $this->render('_comments_form', [
        'comments' => $comments,
    ]);
    ?>
    <?php Pjax::begin(['id' => 'comments']) ?>
    <?php
    foreach ($listComments as $item_comment) {
        ?>
        <div style="background: #D6C6B4; border-radius: 25px; padding: 15px; margin: 5px;">
            <p><?= $item_comment->name; ?></p>
            <p><?= $item_comment->message; ?></p>
        </div>
    <? }
    ?>
    <?php Pjax::end() ?>

</div>
