<?php
/**
 *
 * @var \app\models\Comments $comments
 */

    $this->registerJs(
        '$("document").ready(function(){
            $("#new_comments").on("pjax:end", function() {
            $.pjax.reload({container:"#comments"});  //Reload GridView
        });
    });'
    );
?>

<div class="notes-form">
    <?php yii\widgets\Pjax::begin(['id' => 'new_comments']) ?>
    <?php $form = \yii\widgets\ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

    <?= $form->field($comments, 'name')->textInput() ?>

    <?= $form->field($comments, 'message')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= \yii\helpers\Html::submitButton('Comment', ['class' => $comments->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php \yii\widgets\ActiveForm::end(); ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>