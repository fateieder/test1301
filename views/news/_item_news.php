<?php
/**
 * @var $model \app\models\News
 */
?>
<div>
    <?php
    if ($model->image) { ?>
        <img src="/images/<?= $model->image; ?>" style="width: 200px;">
    <?php } ?>

    <h2>
        <?= $model->preview; ?>
    </h2>
    <p>
        <?= $model->text; ?>
    </p>

</div>
