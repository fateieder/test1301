<?php

namespace app\controllers;

use app\models\Comments;
use Yii;
use app\models\News;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 3,
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param $name
     * @return mixed
     * @throws NotFoundHttpException
     * @internal param string $name
     */
    public function actionView($name)
    {
        $model = $this->findModel($name);
        $comments = new Comments();
        $comments->parent_id = $model->id;
        if ($comments->load(Yii::$app->request->post()) && $comments->save()) {
            $comments = new Comments();
        }
        $listComments = Comments::findAll(['parent_id' => $model->id]);
        return $this->render('view', [
            'model' => $model,
            'comments' => $comments,
            'listComments' => $listComments
        ]);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param $name
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     * @internal param string $name
     */
    protected function findModel($name)
    {
        if (($model = News::findOne(['url_title' => $name])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
