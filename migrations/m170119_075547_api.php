<?php

use yii\db\Migration;

class m170119_075547_api extends Migration
{
    public function safeUp()
    {
        $this->createTable('warehouse', [
            'id' => $this->primaryKey()->unsigned(),
            'article' => $this->integer(8)->unsigned()->notNull(),
            'count' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->timestamp(8)->notNull(). ' DEFAULT NOW()',
            'updated_at' => $this->timestamp(8)->notNull(). ' DEFAULT NOW()',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('warehouse');
    }

}
