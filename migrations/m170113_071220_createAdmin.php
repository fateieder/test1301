<?php

use yii\db\Migration;

class m170113_071220_createAdmin extends Migration
{
    public function up()
    {
        $user = new \app\modules\admin\models\Users();
        $user->login = 'login';
        $user->name = 'login';
        $user->status = \app\modules\admin\models\Users::ACTIVE;
        $user->setPassword('password');
        $user->generateAuthKey();
        
        $user->save();
        
    }

    public function down()
    {
        echo "m170113_071220_createAdmin cannot be reverted.\n";

        return false;
    }
    
}
