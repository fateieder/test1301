<?php

use yii\db\Migration;

class m170113_062917_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey()->unsigned(),
            'login' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->null(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'status' => $this->smallInteger()->unsigned()->defaultValue(0)->notNull(),
            'created_at' => $this->integer()->unsigned()->null(),
            'updated_at' => $this->integer()->unsigned()->null(),
            'last_login' => $this->integer()->unsigned()->null(),
        ]);

        $this->createTable('category',[
            'id' => $this->primaryKey()->unsigned(),
            'parent_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer()->unsigned()->null(),
            'updated_at' => $this->integer()->unsigned()->null(),
        ]);

        $this->createTable('news',[
            'id' => $this->primaryKey()->unsigned(),
            'category_id' => $this->integer()->unsigned()->notNull(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'url_title' => $this->string()->notNull(),
            'image' => $this->string()->null(),
            'preview' => $this->text()->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer()->unsigned()->null(),
            'updated_at' => $this->integer()->unsigned()->null(),
        ]);

        $this->addForeignKey('FK_news_category_id', 'news', 'category_id', 'category', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_news_user_id', 'news', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('users');
        $this->dropTable('category');
        $this->dropTable('news');
    }
}
