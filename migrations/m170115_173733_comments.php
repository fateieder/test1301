<?php

use yii\db\Migration;

class m170115_173733_comments extends Migration
{
    public function up()
    {

        $this->createTable('comments',[
            'id' => $this->primaryKey()->unsigned(),
            'parent_id' => $this->integer()->unsigned()->null(),
            'name' => $this->string()->notNull(),
            'message' => $this->text(),
            'created_at' => $this->integer()->unsigned()->null(),
            'updated_at' => $this->integer()->unsigned()->null(),
        ]);


    }

    public function down()
    {
        $this->dropTable('comments');
    }

}
