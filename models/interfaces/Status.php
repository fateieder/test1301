<?php

namespace app\models\interfaces;

interface Status
{
    const INACTIVE = 0;
    const UNVERIFY = 5;
    const ACTIVE = 10;
    const DELETED = 100;
    
}