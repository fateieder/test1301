<?php

namespace app\models;

use yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "warehouse".
 *
 * @property integer $id
 * @property integer $article
 * @property integer $count
 * @property string $created_at
 * @property string $updated_at
 */
class Warehouse extends yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'warehouse';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article'], 'required'],
            [['article', 'count'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['article'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Article',
            'count' => 'Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
