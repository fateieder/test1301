<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(['id' => 'form', 'options' => ['enctype' => 'multipart/form-data']]);  ?>
    
    <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
        'data' => \app\modules\admin\models\Category::getListAllCategory($model->id),
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
//            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
//            'showCancel' => false,
//            'browseClass' => 'btn btn-primary btn-block',
//            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
//            'browseLabel' => 'Выбрать изображение'
        ],
    ]); ?>

    <?= $form->field($model, 'preview')->textarea(['rows' => 3]) ?>
    <?php echo  Yii::getAlias('@webroot');?>
<!--    --><?//= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,

            //'imageManagerJson' => Url::to(['/admin/news/images-get']),
            'imageUpload' => \yii\helpers\Url::to(['/admin/news/image-upload']),
            'plugins' => [
                //'clips',
                'imagemanager',
                'filemanager',
                'fullscreen'
            ]
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
