<?php

namespace app\modules\admin\models;

use yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property News[] $news
 */
class Category extends \app\models\Category
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['category_id' => 'id']);
    }

    public static function getListAllCategory($id=null) {
        $query = self::find();
        if ($id){
            $query->andWhere(['!=', 'id', $id]);
        }
        return yii\helpers\ArrayHelper::map($query->all(), 'id', 'name');
    }
}
