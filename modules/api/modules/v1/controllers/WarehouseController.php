<?php
namespace app\modules\api\modules\v1\controllers;

use app\modules\api\modules\v1\components\Warehouse\WarehouseLoad;
use app\modules\api\modules\v1\components\Warehouse\WarehouseShip;
use app\modules\api\modules\v1\models\Warehouse;
use yii;


/**
 * ActionsController implements the CRUD actions for Actions model.
 */
class WarehouseController extends yii\rest\ActiveController
{
    public $modelClass = 'app\modules\api\modules\v1\models\Warehouse';
    public $serializer = [
        'class' => 'app\modules\api\modules\v1\serializers\RestSerializer',
//        'collectionEnvelope' => 'categories',
    ];

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => yii\filters\Cors::className(),
        ];
//        $behaviors['authenticator'] = [
//            'class' => yii\filters\auth\HttpBearerAuth::className(),
//            'except' => ['options']
//        ];
        return $behaviors;
    }

    public function actions()
    {
        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }


    public function actionLoad()
    {
        $post = Yii::$app->request->post();
        if (empty($post['goods']) || count($post['goods']) == 0) {
            throw new yii\base\Exception(400, 'Bad request.', 400);
        }
        WarehouseLoad::load($post['goods']);

        return ;
    }

    public function actionShip()
    {
        $post = Yii::$app->request->post();
        if (empty($post['goods']) || count($post['goods']) == 0) {
            throw new yii\base\Exception(400, 'Bad request.', 400);
        }
        
        return WarehouseShip::ship($post['goods']);
    }
}