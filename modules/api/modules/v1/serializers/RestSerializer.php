<?php

namespace app\modules\api\modules\v1\serializers;

use yii\rest\Serializer;

class RestSerializer extends Serializer
{

    public function serialize($data)
    {
        $data = parent::serialize($data);
        if (!empty($data)) {
            $result = array_merge(['timestamp'=> (integer)(time().gettimeofday()['usec'])], $data);
        } else {
            $result = ['timestamp'=> (integer)(time().gettimeofday()['usec'])];
        }

        return $result;
    }

    /**
     * @param \yii\base\Arrayable $model
     * @return array
     */
    protected function serializeModel($model)
    {
        $data = parent::serializeModel($model);
        return $data;
    }

}