<?php
namespace app\modules\api\modules\v1\components\Warehouse;

use yii\db\Query;
use yii\helpers\ArrayHelper;

class WarehouseShip
{
    public static function ship($goods)
    {
        $keys = array_keys($goods);

        $query = new Query();
        $query->from('warehouse');
        $query->where(['article' => $keys]);
        $query->andWhere('count != 0');
        $query->orderBy('created_at');
        $data = $query->all();

        $update_data = [];
        $status = true;
        foreach ($keys as $item) {
            $flag = false;
            foreach ($data as $model) {
                if (($item == $model['article']) && ($goods[$item] != 0)) {
                    if ($model['count'] >= $goods[$item]) {
                        $update_data[] = [
                            'id' => $model['id'],
                            'article' => $model['article'],
                            'timestamp'=> self::datetimeInTimestamp($model['created_at']),
                            'old_count' => $model['count'],
                            'goods' => $goods[$item]
                        ];
                        $goods[$item] = 0;
                        $flag = true;
                        continue;
                    } elseif ($model['count'] < $goods[$item]) {
                        $update_data[] = [
                            'id' => $model['id'],
                            'article' => $model['article'],
                            'timestamp'=> self::datetimeInTimestamp($model['created_at']),
                            'old_count' => $model['count'],
                            'goods' => $model['count']
                        ];
                        $goods[$item] = $goods[$item] - $model['count'];
                    }
                }
            }
            if ($flag == false) {
                $status = false;
            }
        }
        if ($status == true) {
            self::updateShip($update_data);
            return [
                'status' => "shipped",
                'goods' => ArrayHelper::map($update_data, 'timestamp', 'goods', 'article')
            ];
        } else {
            return [
                'status' => 'Not shipped',
                'notEnoughGoods' => self::generateError($goods)
                ];
        }
    }

    protected function updateShip($update_array)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        foreach ($update_array as $item) {
            \Yii::$app->db->createCommand()->update('warehouse', [
                'count' => $item['old_count'] - $item['goods'],
                'updated_at' => 'NOW()'
            ], ['id' => $item['id']])->execute();
        }
        $transaction->commit();
        return [];
    }

    protected function generateError($goods){
        $error = [];
        foreach ($goods as $key=>$item) {
            if ($item !=0) {
                $error[] = [
                    $key => $item
                ];
            }
        }
        return $error;
    }

    private function datetimeInTimestamp($date){
        $data = new \DateTime($date);
        return (integer)$data->format('Uu');
    }
}