<?php
namespace app\modules\api\modules\v1\components\Warehouse;

class WarehouseLoad
{
    public static function load($goods)
    {
        $keys = array_keys($goods);
        $insert = [];

        foreach ($keys as $item) {
            $insert[] = [$item, $goods[$item]];
        }

        $transaction = \Yii::$app->db->beginTransaction();
        /* используем функцию множественой вставки batchInsert */
        if (!empty($insert)) {
            \Yii::$app->db->createCommand()->batchInsert('warehouse', ['article', 'count'], $insert)->execute();
        }
        $transaction->commit();
    }
}